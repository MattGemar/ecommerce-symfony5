<?php

namespace App\Taxes;

use Cocur\Slugify\Slugify;
use Psr\Log\LoggerInterface;

class Calculator
{
    protected $logger;

    public function __construct(LoggerInterface $logger, Slugify $slugify)
    {
        $this->logger =  $logger;
    }

    public function calcul(float $prix): float
    {
        $this->logger->error("La tva est calculée");
        return $prix * 20 / 100;
    }
}
