<?php

namespace App\Cart;

use App\Cart\CartItem;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CartService
{
    protected $session;
    protected $productRepository;

    public function __construct(SessionInterface $session, ProductRepository $productRepository)
    {
        $this->session = $session;
        $this->productRepository = $productRepository;
    }

    protected function getCart(): array
    {
        return $this->session->get('cart', []);
    }

    protected function saveCart(array $cart)
    {
        $this->session->set('cart', $cart);
    }

    public function remove(int $id)
    {
        //1. Retrouver le panier dans la session
        //2. Si il n'existe pas en créer un nouveau
        $cart = $this->getCart();
        //3. On supprime l'objet du panier
        unset($cart[$id]);
        //4. On enregiste le panier dans la session
        $this->saveCart($cart);
    }

    public function add(int $id)
    {
        $cart = $this->getCart();
        if (!array_key_exists($id, $cart)) {
            $cart[$id] = 0;
        }
        $cart[$id]++;
        $this->saveCart($cart);
    }

    public function decrement(int $id)
    {
        //1. Retrouver le panier dans la session
        //2. Si il n'existe pas en créer un nouveau
        $cart = $this->getCart();
        //3. Voir si le produit existe dans le panier
        //4. Si la quantité est de 1 on supprime le produit de panier
        //5. Si le produit est présent plus d'une fois on diminue la quantité
        if (!array_key_exists($id, $cart)) {
            return;
        }

        if ($cart[$id] === 1) {
            $this->remove($id);
            return;
        }

        $cart[$id]--;
        //6. On enregiste le panier dans la session
        $this->saveCart($cart);
    }

    public function getTotal(): int
    {
        $total = 0;
        foreach ($this->getCart() as $id => $qty) {
            $product = $this->productRepository->find($id);
            if (!$product) {
                continue;
            }
            $total += $product->getPrice() * $qty;
        }
        return $total;
    }

    /**
     *
     * @return array<CartItem>
     */
    public function getDetailedCartItems(): array
    {
        $detailedCart = [];
        foreach ($this->getCart() as $id => $qty) {
            $product = $this->productRepository->find($id);
            if (!$product) {
                continue;
            }
            $detailedCart[] = new CartItem($product, $qty);
        }
        return $detailedCart;
    }

    public function empty()
    {
        $this->saveCart([]);
    }
}
