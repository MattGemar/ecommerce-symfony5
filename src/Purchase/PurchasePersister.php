<?php

namespace App\Purchase;

use App\Cart\CartService;
use DateTime;
use App\Entity\Purchase;
use App\Entity\PurchaseItem;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class PurchasePersister
{
    protected $security;
    protected $cartService;
    protected $manager;

    public function __construct(Security $security, CartService $cartService, EntityManagerInterface $manager)
    {
        $this->security = $security;
        $this->cartService = $cartService;
        $this->manager = $manager;
    }

    public function storePurchase(Purchase $purchase)
    {
        //6. lier la purchase à l'utilisateur
        $purchase->setUser($this->security->getUser());

        $this->manager->persist($purchase);
        //7. lier avec les produits du panier
        foreach ($this->cartService->getDetailedCartItems() as $cartItem) {
            $purchaseItem = new PurchaseItem();
            $purchaseItem->setPurchase($purchase)
                ->setProduct($cartItem->product)
                ->setProductName($cartItem->product->getName())
                ->setProductPrice($cartItem->product->getPrice())
                ->setTotal($cartItem->getTotal())
                ->setQuantity($cartItem->qty);
            $this->manager->persist($purchaseItem);
        }
        //8. Enregistrer la commande
        $this->manager->flush();
    }
}
