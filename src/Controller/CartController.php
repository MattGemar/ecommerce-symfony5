<?php

namespace App\Controller;

use App\Cart\CartService;
use App\Form\CartConfirmationType;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class CartController extends AbstractController
{
    protected $productRepository;
    protected $cartService;

    public function __construct(ProductRepository $productRepository, CartService $cartService)
    {
        $this->productRepository = $productRepository;
        $this->cartService = $cartService;
    }

    /**
     * @Route("/cart/", name="cart_show")
     */
    public function show(): Response
    {
        $form = $this->createForm(CartConfirmationType::class);

        $detailedCart = $this->cartService->getDetailedCartItems();
        $total = $this->cartService->getTotal();
        return $this->render('cart/index.html.twig', [
            'items' => $detailedCart,
            'total' => $total,
            'confirmationForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/cart/delete/{id}", name="cart_delete", requirements={"id": "\d+"})
     */
    public function delete($id): Response
    {
        //0. verifier si l'id correspond bien à un produit
        $product = $this->productRepository->find($id);
        if (!$product) {
            throw $this->createNotFoundException("Le produit $id n'existe pas et en peut pas être supprimé.");
        }
        //1. Appel du service pour supprimer l'article du panier
        $this->cartService->remove($id);

        //2. Création du message de succès
        $this->addFlash('success', "Le produit à été retiré du panier.");

        return $this->redirectToRoute('cart_show', []);
    }

    /**
     * @Route("/cart/add/{id}", name="cart_add", requirements={"id": "\d+"})
     */
    public function add($id, Request $request): Response
    {
        //0. verifier si l'id correspond bien à un produit
        $product = $this->productRepository->find($id);
        if (!$product) {
            throw $this->createNotFoundException("Le produit $id n'existe pas");
        }
        //1. appel au service pour décrémenter le produit au panier
        $this->cartService->add($id);
        //2. Création du message de succès
        $this->addFlash('success', "Le produit à été ajouté au panier.");
        //3. Redirection vers le produit à ajouter
        if ($request->query->get('returnToCart')) {
            return $this->redirectToRoute('cart_show', []);
        }
        return $this->redirectToRoute('product_show', [
            'slug' => $product->getSlug(),
            'category_slug' => $product->getCategory()->getSlug()
        ]);
    }

    /**
     * @Route("/cart/decrement/{id}", name="cart_decrement", requirements={"id": "\d+"})
     */
    public function decrement($id): Response
    {
        //0. verifier si l'id correspond bien à un produit
        $product = $this->productRepository->find($id);
        if (!$product) {
            throw $this->createNotFoundException("Le produit $id n'existe pas");
        }
        //1. appel au service pour ajout du produit au panier
        $this->cartService->decrement($id);
        //2. Création du message de succès
        $this->addFlash('success', "Le produit à été supprimé du panier.");
        //3. Redirection vers le produit à ajouter
        return $this->redirectToRoute('cart_show', []);
    }
}
