<?php

namespace App\Controller\Purchase;

use App\Entity\User;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class PurchasesListController extends AbstractController
{
    /**
     * @Route("/purchases", name="purchase_index")
     * @IsGranted("ROLE_USER", message="Vous devez être connecté pour acceder à vos commandes")
     */
    public function index()
    {
        //1. On doit s'assurer que la personne est connecté sinon redirection vers connexion
        /** @var User */
        $user = $this->getUser();
        //2. On doit savoir qui est connecté
        //3. On doit donner l'utilisateur à twig
        return $this->render('purchase/index.html.twig', [
            'purchases' => $user->getPurchases(),
        ]);
    }
}
