<?php

namespace App\Controller\Purchase;

use DateTime;
use App\Entity\Purchase;
use App\Cart\CartService;
use App\Entity\PurchaseItem;
use App\Form\CartConfirmationType;
use App\Purchase\PurchasePersister;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToArrayTransformer;

class PurchaseConfirmationController extends AbstractController
{
    protected $cartService;
    protected $manager;
    protected $persister;

    public function __construct(CartService $cartService, EntityManagerInterface $manager, PurchasePersister $persister)
    {
        $this->cartService = $cartService;
        $this->manager = $manager;
        $this->persister = $persister;
    }

    /**
     * @Route("/purchase/confirm", name="purchase_confirm")
     * @IsGranted("ROLE_USER", message="Vous devez être connecté pour confirmer une commande")
     */
    public function confirm(Request $request)
    {
        //1. Lire les données du formulaire
        $form = $this->createForm(CartConfirmationType::class);
        $form->handleRequest($request);
        //2. Si le formulaire n'est pas soumis: sortir
        if (!$form->isSubmitted()) {
            //Message flash + redirection
            $this->addFlash('warning', 'Vous devez remplir le formulaire');
            return $this->redirectToRoute('cart_show');
        }
        //3. Si il n'ya pas de produit: sortir
        $cartItems = $this->cartService->getDetailedCartItems();

        if (count($cartItems) === 0) {
            $this->addFlash('warning', 'Vous ne pouvez pas confirmer une commande vide');
            return $this->redirectToRoute('cart_show');
        }
        //4. création de la purchase
        /**
         * @var Purchase
         */
        $purchase = $form->getData();
        $this->persister->storePurchase($purchase);
        return $this->redirectToRoute('purchase_payment_form', [
            'id' => $purchase->getId()
        ]);
    }
}
